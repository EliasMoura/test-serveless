module.exports.main = (event, context, callback) => {

  const response =  {
    statusCode: 200,
    body: JSON.stringify({
      message: `Hello, the current time is ${new Date().toTimeString()}.`,
    }),
  };

  callback(null, response);
};

const successTransactions = (transactions) => {
  let transactionsSucceeded = transactions.filter(transaction => transaction.status === 'succeeded');
  return transactionsSucceeded
}

const transactionsXCurrency = (transactions, currency) => {
  let result = transactions.filter(transaction => transaction.currency === `${currency}`);
  return result;
}

const calculateBalance = (transactions) => { 
  let result = transactions.reduce((acc, {amout}) => acc + amout, 0)
  return result;
}

module.exports.transactions = (event, context, callback) => {
  const {transactions} = JSON.parse(event.body);
  let succeeded = successTransactions(transactions);

  let transactionsARS = transactionsXCurrency(succeeded, 'ARS');
  let transactionUSD = transactionsXCurrency(succeeded, 'USD');

  let totalARS = calculateBalance(transactionsARS);
  let totalUSD = calculateBalance(transactionUSD);

  console.log(totalARS, totalUSD)
  let balance = {
    "balance": {
      "amounts": {
        "ars": `${totalARS}`,
        "usd": `${totalUSD}`
      }
    }
  }

  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: `Hello.`,
    }),
  };

  callback(null, response);
};

module.exports.hello = async event => {
  return {
    statusCode: 200,
    headers: {
      // Uncomment the line below if you need access to cookies or authentication
      // 'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(
      {
        message: 'Your function executed successfully!'
      },
      null,
      2
    ),
  };
};